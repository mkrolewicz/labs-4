﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;

namespace Lab4.Component2
{
    public class Component2 : ComponentFramework.AbstractComponent, RequiredInterface
    {
        public Component2()
        {
            RegisterProvidedInterface<Contract.RequiredInterface>(this);
        }

        public void metoda1()
        {
            Console.WriteLine("metoda1");
        }
        public void metoda2()
        {
            Console.WriteLine("metoda2");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
