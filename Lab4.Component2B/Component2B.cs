﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Component2B : ComponentFramework.AbstractComponent, RequiredInterface 
    {
         public Component2B()
        {
            RegisterProvidedInterface<Contract.RequiredInterface>(this);
        }

        public void metoda1()
        {
            Console.WriteLine("blabla");
        }
        public void metoda2()
        {
            Console.WriteLine("bleble");
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
