﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class Component1 : AbstractComponent, RequiredInterface
    {
        RequiredInterface reqiuredInterface;

        public Component1(RequiredInterface reqiuredInterface)
        {
            this.reqiuredInterface = reqiuredInterface;
            RegisterProvidedInterface<RequiredInterface>(this.reqiuredInterface);
        }

        public Component1()
        {
            RegisterRequiredInterface<RequiredInterface>();
        }

        public override void InjectInterface(Type type, object impl)
        {
        }


        public void metoda1()
        {
            reqiuredInterface.metoda1();
        }

        public void metoda2()
        {
            reqiuredInterface.metoda2();
        }
    }
}
